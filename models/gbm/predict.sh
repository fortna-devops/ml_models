#!/usr/bin/env bash

PAYLOAD=$1
CONTENT=${2:-application/json}

curl --data-binary @${PAYLOAD} -H "Content-Type: ${CONTENT}" -v http://localhost:8080/invocations
