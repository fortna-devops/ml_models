#!/usr/bin/env bash

docker build \
    --build-arg ssh_prv_key="$(cat ../../build_args/ssh/prv_key)" \
    --build-arg ssh_pub_key="$(cat ../../build_args/ssh/pub_key)" \
    --build-arg aws_credentials="$(cat ../../build_args/aws/credentials)" \
    --build-arg aws_config="$(cat ../../build_args/aws/config)" \
    -t gbm:latest .
