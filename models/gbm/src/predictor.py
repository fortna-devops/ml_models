import logging
import configargparse

import flask

import config
from model import Model


parser = configargparse.ArgumentParser()
parser.add_argument('--results_key', env_var='RESULTS_KEY',
                    default='timeseries_predictions',
                    help='path to save results '
                         '(default is "timeseries_predictions")')
parser.add_argument('--work_dir', env_var='WORK_DIR', default='/opt/ml/',
                    help='path to working dir (default is "/opt/ml/")')
parser.add_argument('--log_level', env_var='LOG_LEVEL', default=logging.INFO,
                    help='logging level (default is "INFO")')

args, _ = parser.parse_known_args()
config.config.init(**vars(args))


logger = logging.getLogger()


app = flask.Flask(__name__)
model = Model()
model.load()


@app.route('/ping', methods=['GET'])
def ping():
    """
    Determine if the container is working and healthy.
    """
    logger.info('Ping request')
    response = model.get_health()
    return flask.Response(**response.to_dict())


@app.route('/invocations', methods=['POST'])
def invoke():
    """
    Do an inference on a single batch of data.
    """
    logger.info('Invoke request')
    response = model.predict(flask.request.data)
    logger.info('Response %s', response.response)
    return flask.Response(**response.to_dict())
