__all__ = ['chunks']


def chunks(l, n=100):
    """
    Yield successive n-sized chunks from l.
    """
    for i in range(0, len(l), n):
        yield l[i:i + n]
