import json
import os
import pickle
import logging
import threading
import traceback
import queue
import datetime as dt
from typing import Optional, Tuple, List, Type, Any, Dict, Iterable

import pandas as pd
import boto3
import s3fs

from mhs_rdb import Sensors, PredictedDataHour, PredictedDataMinute, PredictedVFDDataMinute, \
    PredictedVFDDataHour, PredictedVFDTempDataMinute, PredictedVFDTempDataHour, \
    ConfigManager
from mhs_rdb.session import Session
from mhs_rdb.utils.data import DataProxy
from mhs_rdb.models import SensorsTypeEnum
from mhs_rdb.services.base_data_source import DataKind

from response import PredictionResponse
from config import config
from utils import chunks


logger = logging.getLogger()


class Model:

    prediction_range_hrs = 2
    min_sampling = 'min'
    hr_sampling = 'hour'

    VFD_DATA = 'vfd_data'
    VFD_TEMP = 'vfd_temp'
    SENSOR_DATA = 'sensor_data'

    def __init__(self, boto_session: Optional[boto3.Session] = None,
                 debug: Optional[bool] = False):
        self._results_key = config.results_key
        self._debug = debug

        self._last_error = None
        if boto_session is None:
            boto_session = boto3.Session()
        self._boto_session = boto_session

        env = os.getenv('env')
        assert env, '"env" variable not set'

        self._bucket_name_prefix = 'mpm-ml-models-prod' if env == 'master' else 'mpm-ml-models-develop2'
        self.model_path_template = 's3://{bucket_prefix}/{site}'

        self._config_manager = ConfigManager(env, boto_session=self._boto_session)

        self._s3fs = s3fs.S3FileSystem(session=self._boto_session)
        self._models = {}

        self._queue = queue.Queue()
        self._predictor = threading.Thread(target=self._background_predict, name='predictor',
                                           daemon=True)
        self._predictor.start()


    def get_table_name(self, sensor: Sensors.model, attr: str) -> str:
        if sensor.type == SensorsTypeEnum.vfd:
            table_names = {
                'speed': self.VFD_DATA,
                'current': self.VFD_DATA,
                'voltage': self.VFD_DATA,
                'mechanical_power': self.VFD_DATA,
                'temperature': self.VFD_TEMP,
            }
            return table_names[attr]
        return self.SENSOR_DATA

    def load(self):
        pass

    def get_db_service(self, table: str, sampling: str) -> Any:
        services = {
            self.VFD_DATA: {self.min_sampling: PredictedVFDDataMinute, self.hr_sampling: PredictedVFDDataHour},
            self.VFD_TEMP: {self.min_sampling: PredictedVFDTempDataMinute, self.hr_sampling: PredictedVFDTempDataHour},
            self.SENSOR_DATA: {self.min_sampling: PredictedDataMinute, self.hr_sampling: PredictedDataHour}
        }
        return services[table][sampling]

    def train(self):
        # No training in this implementation
        raise NotImplementedError('Training is not available for this algorithm')

    def get_health(self) -> PredictionResponse:
        predictor_is_alive = self._predictor.is_alive()

        if predictor_is_alive:
            status = 200
            response = '\n'
        else:
            status = 404
            response = f'Predictor thread alive: {predictor_is_alive}\n' \
                f'Last error:\n' \
                f'{traceback.format_exc(self._last_error)}'
        return PredictionResponse(response, status)

    def load_site_models(self, customer: str, site: str) -> None:
        top_dir = self.model_path_template.format(bucket_prefix=self._bucket_name_prefix, site=site)
        self._models[(customer, site)] = {}
        if not self._s3fs.exists(top_dir):
            logger.info(f'There are no models at {top_dir}')
            return None

        for model_path in self._s3fs.ls(top_dir):

            file_name = model_path.split('/')[-1]
            sensor_id = int(file_name.split('_')[1])

            with self._s3fs.open(model_path, 'rb') as f:
                model_and_metadata = pickle.load(f)

            model = pickle.loads(model_and_metadata['model'])

            metadata = model_and_metadata['metadata']
            model_name = os.path.basename(model_path)


            if sensor_id not in self._models[(customer, site)]:
                logger.debug(f'sensor_id: {sensor_id} is not in models dict')
                self._models[(customer, site)][sensor_id] = {'sensor_models': [], 'query_tags': set()}
            self._models[(customer, site)][sensor_id]['sensor_models'].append((model_name, model, metadata))
            self._models[(customer, site)][sensor_id]['query_tags'].update(metadata['query_tags'])
        logger.info('finished loading all models for all sensors')

    def get_sensor_models(self, customer: str, site: str, sensor_id: int) -> list:
        return self._models[(customer, site)].get(sensor_id)

    def _background_predict(self) -> None:
        while True:
            if self._queue.empty():
                continue
            data = self._queue.get()
            try:
                self._predict(data)
            except BaseException as e:
                self._last_error = e
                logger.error('Error in predictor: ', exc_info=e)
            finally:
                self._queue.task_done()

    def predict(self, request: bytes) -> PredictionResponse:
        request = request.decode('utf-8')
        logger.info(f'Request: {request}')
        self._queue.put(json.loads(request))

        if self._debug:
            # wait for background predictor
            self._queue.join()

        return PredictionResponse('Task accepted')

    def get_conveyor_data(self, conveyor_id: int, date_range: Tuple[dt.datetime, dt.datetime],
                          metrics: Iterable, db_session: Session):
        data = DataProxy(db_session).get_merged_by_conveyor(
            Sensors.model.conveyor_id == conveyor_id,
            start_datetime=date_range[0],
            end_datetime=date_range[1],
            sample_rate='minute',
            metrics=metrics,
            data_kinds=(DataKind.measured,),
        )
        return data

    def format_data(self, data):
        aggregated_df = pd.DataFrame(data)
        dt_format = '%Y-%m-%d %H:%M:%S'
        aggregated_df['timestamp'] = pd.to_datetime(aggregated_df['timestamp'], format=dt_format)
        aggregated_df.set_index(pd.DatetimeIndex(aggregated_df['timestamp']), inplace=True)
        return aggregated_df

    def _predict(self, data: Dict[str, Any]) -> None:
        customer = data['customer_name']
        site = data['site_name']

        logger.info('Get date range')
        now = dt.datetime.utcnow().replace(minute=0, second=0, microsecond=0)
        range_start = now - dt.timedelta(hours=self.prediction_range_hrs)
        range_end = now - dt.timedelta(minutes=1)
        date_range = (range_start, range_end)
        logger.info(f'Date range: {date_range}')

        logger.info(f'Load all models for {site}')
        self.load_site_models(customer, site)

        with self._config_manager.connector.get_session(customer, site) as db_session:
            sensors = Sensors(db_session).get_all()
            logger.info(f'Got {len(sensors)} sensors from RDS')

            for sensor in sensors:
                logger.info(f'Processing sensor id "{sensor.id}"')

                """
                NOTE:
                Skipping VFD sensors for now.
                Training algo needs to be tweaked to account for VFDs.
                Current algo assumes multiple sensors per conveyor,
                however VFDs only have 1 sensor per conveyor.
                """
                if sensor.type == SensorsTypeEnum.vfd:
                    logger.info('sensor is VFD sensor, skipping')
                    continue

                predictions_minute, predictions_hour = self.predict_for_sensor(
                    sensor, date_range, customer, site, db_session)
                if not all([predictions_minute, predictions_hour]):
                    continue

                logger.info(predictions_minute)
                logger.info(predictions_hour)
                self.write_data(predictions_minute, self.min_sampling, db_session)
                self.write_data(predictions_hour, self.hr_sampling, db_session)

        logger.info('All done predicting for all sensors')

    def predict_for_sensor(self, sensor, date_range, customer, site,
                           db_session) -> Tuple[Optional[pd.DataFrame], Optional[pd.DataFrame]]:
        """
        Calculate predictions_minute and predictions_hour for given sensor
        """
        logger.info(f'predict_for_sensor {sensor.id}, date_range: {date_range}')

        logger.info('Load model')
        predict_model = self.get_sensor_models(customer, site, sensor.id)
        if predict_model is None:
            logger.warning(f'There are no models for sensor id "{sensor.id}"')
            return None, None

        logger.info('Get data')
        metrics = predict_model['query_tags']
        ts_data = self.get_conveyor_data(sensor.conveyor_id, date_range, metrics, db_session)
        if len(ts_data) == 0:
            return (None, None)
        input_data = self.format_data(ts_data)

        logger.info('Make predictions')
        predictions = {}
        # iterate over available models for conveyor
        for model_name, model, meta in predict_model['sensor_models']:

            s_id = int(meta['sensor_id'])
            model_input_keys = [key for key in meta['model_input_datarange'].keys()]
            model_input = input_data.loc[:, model_input_keys]

            if model_input.empty:
                logger.warning(f'Model "{model_name}" gets empty input')
                continue

            # do prediction
            try:
                pred = model.predict(model_input)
            except ValueError:
                logger.warning(f'Error for model "{model_name}"')
                continue

            # format output
            _, attribute = meta['model_target'].split('.')
            target_colname_actual = meta['model_target']
            prediction = pd.DataFrame({attribute: pred}, index=model_input.index)
            prediction[f'{attribute}_residuals'] = \
                    prediction[attribute] - input_data[target_colname_actual]

            table = self.get_table_name(sensor, attribute)
            if table not in predictions:
                predictions[table] = []
            predictions[table].append(prediction)

        if not predictions.values():
            logger.warning(f'No predictions for sensor id "{s_id}"')
            return (None, None)

        logger.info('Concat predictions')
        predictions_minute = {}
        predictions_hour = {}
        for table_name, pred_data in predictions.items():
            logger.info(pred_data)
            data_vals = pd.concat(pred_data, sort=False, axis=1)
            data_vals['sensor_id'] = s_id
            data_vals['timestamp'] = data_vals.index
            predictions_minute[table_name] = data_vals
            predictions_hour[table_name] = self.resample(data_vals, self.hr_sampling, sensor_id=s_id)

        return predictions_minute, predictions_hour

    def resample(self, data: pd.DataFrame, rate: str, **static_data) -> pd.DataFrame:
        rule = 'H' if self.hr_sampling else 'min'
        prediction = data.resample(rule).mean()
        static_data.setdefault('timestamp', prediction.index)
        for key, value in static_data.items():
            prediction[key] = value
        return prediction

    def convert_to_rds(self, data: pd.DataFrame, model_cls: Type[Any]) -> List[Any]:
        rows = []
        for _, row in data.iterrows():
            if row.isnull().values.any():  # Skip rows with NaN values
                continue
            model = model_cls(**row)
            rows.append(model)
        return rows

    def write_data(self, data: Dict[str, pd.DataFrame], sampling: str, db_session) -> None:
        """
        Write pandas dataframe to RDS
        """
        assert sampling in {self.min_sampling, self.hr_sampling}, \
            'invalid value for "sampling" argument'
        logger.info(f'data to write: {data}')
        for table, data in data.items():
            service = self.get_db_service(table, sampling)(db_session)
            data2write = self.convert_to_rds(data, service.model)
            total_wrote = 0
            for row in chunks(data2write):
                service.insert_all(row)
                total_wrote += len(row)
                logger.info(f'Wrote {total_wrote}/{len(data2write)} ({round(total_wrote/len(data2write)*100, 2)}%)')
