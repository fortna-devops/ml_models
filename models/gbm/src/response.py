class PredictionResponse(object):
    """
    Simple response wrapper for predictions
    """
    def __init__(self, response, status=200, mimetype='text/plain'):
        self.response = response
        self.status = status
        self.mimetype = mimetype

    def to_dict(self):
        return {
            'response': self.response,
            'status': self.status,
            'mimetype': self.mimetype
        }
