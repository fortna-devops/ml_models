import os
import logging.config


__all__ = ['ConfigError', 'Config', 'config']


class ConfigError(Exception):
    pass


class Config(object):

    def __init__(self):
        self._inited = False
        self._config = {}

    def __getattr__(self, item):
        if not self._inited:
            raise ConfigError('Config is not inited')

        return self._config[item]

    @property
    def inited(self):
        return self._inited

    def init(self, work_dir, log_level, **kwargs):
        input_path = os.path.join(work_dir, 'input', 'data')
        output_path = os.path.join(work_dir, 'output')
        model_path = os.path.join(work_dir, 'model')
        param_path = os.path.join(work_dir, 'input', 'config', 'hyperparameters.json')

        logging_config = {
            'version': 1,
            'disable_existing_loggers': False,
            'formatters': {
                'standard': {
                    'format': '[%(levelname)s][%(module)s] %(asctime)s: %(message)s'
                },
            },
            'handlers': {
                'console': {
                    'level': 'DEBUG',
                    'formatter': 'standard',
                    'class': 'logging.StreamHandler'
                },
                'info_file': {
                    'level': 'DEBUG',
                    'formatter': 'standard',
                    'class': 'logging.FileHandler',
                    'filename': os.path.join(output_path, 'info'),
                    'delay': True
                },
                'failure_file': {
                    'level': 'ERROR',
                    'formatter': 'standard',
                    'class': 'logging.FileHandler',
                    'filename': os.path.join(output_path, 'failure'),
                    'delay': True
                }
            },
            'loggers': {
                '': {
                    'handlers': ['console', 'info_file', 'failure_file'],
                    'level': log_level,
                    'propagate': True
                }
            }
        }

        os.makedirs(output_path, exist_ok=True)
        logging.config.dictConfig(logging_config)

        _config = {
            'INPUT_PATH': input_path,
            'OUTPUT_PATH': output_path,
            'MODEL_PATH': model_path,
            'PARAM_PATH': param_path,
        }

        self._config = dict(_config, **kwargs)
        self._inited = True


config = Config()
