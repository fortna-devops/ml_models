#!/usr/bin/env bash

image=${1:-gbm:latest}

mkdir -p test/model
mkdir -p test/output

rm test/model/*
rm test/output/*

docker run -v $(pwd)/test:/opt/ml --rm ${image} train
