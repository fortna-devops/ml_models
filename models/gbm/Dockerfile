FROM ubuntu:18.04

ENV DEBIAN_FRONTEND noninteractive
ENV PATH=/opt/program:${PATH}

ARG ssh_prv_key
ARG ssh_pub_key

ARG aws_credentials
ARG aws_config

# Setup requirements
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
RUN apt-get install -y gcc git curl wget nginx python3-dev python3-pip && \
    update-alternatives --install /usr/bin/python python /usr/bin/python3.6 1 && \
    update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 1 && \
    pip install --no-cache-dir -U pip flask==1.0.2 gunicorn==19.9.0


# Authorize SSH Host
RUN mkdir -p /root/.ssh && \
    chmod 0700 /root/.ssh && \
    ssh-keyscan bitbucket.org > /root/.ssh/known_hosts

# Add the keys and set permissions
RUN echo "$ssh_prv_key" > /root/.ssh/id_rsa && \
    echo "$ssh_pub_key" > /root/.ssh/id_rsa.pub && \
    chmod 600 /root/.ssh/id_rsa && \
    chmod 600 /root/.ssh/id_rsa.pub


# Add AWS credentials
RUN mkdir -p /root/.aws && \
    chmod 0700 /root/.aws && \
    echo "$aws_credentials" > /root/.aws/credentials && \
    echo "$aws_config" > /root/.aws/config


COPY ./src /opt/program
RUN pip install --no-cache-dir -r /opt/program/requirements.txt && \
    find /opt/program -name \*.pyc -delete


WORKDIR /opt/program
