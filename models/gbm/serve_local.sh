#!/usr/bin/env bash

IMAGE=${1:-gbm:latest}
ENV='rnd'
AWS_RDB_USERNAME='developer'

docker run -v $(pwd)/test:/opt/ml -p 8080:8080 -e env=${ENV} -e AWS_RDB_USERNAME=${AWS_RDB_USERNAME} --rm ${IMAGE} serve
