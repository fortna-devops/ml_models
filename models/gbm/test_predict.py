import boto3

ENDPOINT_NAME = 'gbm-develop'
CONTENT_TYPE = 'application/json'
PROFILE = 'MHS'
PAYLOAD = 'test_payload.json'

session = boto3.Session(profile_name=PROFILE)
runtime_client = session.client('sagemaker-runtime')


def do_predict(data, endpoint_name=ENDPOINT_NAME, content_type=CONTENT_TYPE):
    response = runtime_client.invoke_endpoint(EndpointName=endpoint_name,
                                              ContentType=content_type,
                                              Body=data)
    for line in response['Body'].read().decode().splitlines():
        print(line)


if __name__ == '__main__':
    with open(PAYLOAD) as f:
        do_predict(f.read())
